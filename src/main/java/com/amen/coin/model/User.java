package com.amen.coin.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;



@Entity
@Table(name = "app_user")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
    @Id
    @Column(name="app_user_id")
    @GeneratedValue
    private long id;

    @Column
    private String login;

    @Column
    private String passwordHash;

    @OneToMany
    private List<Transaction> transactionList;

    @OneToOne(cascade = CascadeType.PERSIST)
    private UserData userData;

    public User() {
        this.id = 0L;
        this.transactionList = new ArrayList<>();
    }

    public User(String login, String passwordHash) {
        this.id = 0L;
        this.login = login;
        this.passwordHash = passwordHash;
        this.transactionList = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                '}';
    }

    public void addTransaction(Transaction transaction) {
        transactionList.add(transaction);
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }
}
