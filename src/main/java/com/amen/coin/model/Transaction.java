package com.amen.coin.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_transaction")
public class Transaction {
    @Id
    @Column
    private Long id;

    @Column
    private Long userIdFrom;

    @Column
    private Long userIdTo;

    @Column
    private Double amount;

    public Transaction() {
        this.id = 0L;
    }

    public Transaction(Long userIdFrom, Long userIdTo, Double amount) {
        this.id = 0L;
        this.userIdFrom = userIdFrom;
        this.userIdTo = userIdTo;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserIdFrom() {
        return userIdFrom;
    }

    public void setUserIdFrom(Long userIdFrom) {
        this.userIdFrom = userIdFrom;
    }

    public Long getUserIdTo() {
        return userIdTo;
    }

    public void setUserIdTo(Long userIdTo) {
        this.userIdTo = userIdTo;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", userIdFrom=" + userIdFrom +
                ", userIdTo=" + userIdTo +
                ", amount=" + amount +
                '}';
    }
}
