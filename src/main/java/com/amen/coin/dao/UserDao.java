package com.amen.coin.dao;

import com.amen.coin.model.Transaction;
import com.amen.coin.model.User;
import com.amen.coin.model.UserData;
import com.sun.istack.internal.NotNull;

import java.util.Optional;

public interface UserDao {
    void registerUser(User u, UserData data);

    Optional<User> findById(@NotNull Long id);

    boolean userExists(@NotNull String withLogin);

    void persistTransaction(@NotNull User user, @NotNull Transaction transaction);
}
