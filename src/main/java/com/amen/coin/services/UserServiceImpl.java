package com.amen.coin.services;

import com.amen.coin.dao.UserDao;
import com.amen.coin.model.Transaction;
import com.amen.coin.model.User;
import com.amen.coin.model.UserData;
import com.sun.istack.internal.NotNull;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);
//    private List<User> userList = new ArrayList<>(
//            Arrays.asList(
//                    new User("amen", "kot"),
//                    new User("amen2", null),
//                    new User(null, "kot4"),
//                    new User("amen4", "kot3"))
//    );

    @Override
    public boolean userExists(String login) {
//        LOG.debug("Checking if user : " + login + " exists.");
//        return userList.stream().
//                filter(user -> user.getLogin().equalsIgnoreCase(login)).
//                count() > 0;
        return userDao.userExists(login);
    }

    @Override
    public boolean registerUser(User userToRegister, UserData data) {
        userDao.registerUser(userToRegister, data);
        return true;
    }

    @Override
    public List<User> getAllUsers() {
//        LOG.debug("Getting all users.");
//        return userList;
        return null;
    }

    @Override
    public boolean userLogin(String login, String passwordHash) {
        // TODO: do zrobienia logowanie użytkowników
        throw new NotImplementedException();
//        return false;
    }

    @Override
    public void postSomething(String post, User sender) {

    }

    @Override
    public Optional<User> getUserWithId(Long id) {
        return userDao.findById(id);
    }

    @Override
    public void addTransaction(Long id, Long otherUserId, Double amount) {
        Optional<User> user = getUserWithId(id);
        if(user.isPresent()){
            User u = user.get();
            userDao.persistTransaction(u, new Transaction(id, otherUserId, amount));
        }
    }
}
