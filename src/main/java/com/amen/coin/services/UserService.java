package com.amen.coin.services;

import com.amen.coin.model.User;
import com.amen.coin.model.UserData;
import com.sun.istack.internal.NotNull;

import java.util.List;
import java.util.Optional;

public interface UserService {
    // user story - chce sprawdzić czy dany użytkownik (o danym loginie) już istnieje, więc
    // potrzebuje metody do sprawdzania tego:
    boolean userExists(String login);

    // user story - chcę zarejestrować użytkownika
    boolean registerUser(User userToRegister, UserData data);

    // user story - chcę wylistować wszystkich użytkowników
    List<User> getAllUsers();

    // user story - logowanie użytkownika
    boolean userLogin(String login, String passwordHash);

    void postSomething(String post, @NotNull User sender);

    Optional<User> getUserWithId(Long id);

    void addTransaction(Long id, Long otherUserId, Double amount);
}
